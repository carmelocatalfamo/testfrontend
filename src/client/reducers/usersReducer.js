import * as actions from '../actions/types'

export default (state = [], action) => {
  switch (action.type) {
    case actions.GET_USERS_LIST_SUCCEEDED:
      return action.users

    default:
      return state
  }
}
