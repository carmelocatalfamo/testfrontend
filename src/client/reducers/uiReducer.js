import { combineReducers } from 'redux'

import * as actions from '../actions/types'

export default combineReducers({
  modal: (state = { open: false, type: '' }, action) => {
    switch (action.type) {
      case actions.OPEN_MODAL:
        return { open: true, type: action.modal }

      case actions.CLOSE_MODAL:
        return { open: false, type: '' }

      default:
        return state
    }
  }
})
