import moment from 'moment'
import { range } from 'lodash'

import * as actions from '../actions/types'

moment.locale('it')

const now = moment()

const getDaysOfWeek = (date) => {
  let day = date.day() || 7
  let daysBeforeToday = range(1, day)
  let daysAfterToday = range(day + 1, 8)
  let daysBefore = daysBeforeToday.map((d, idx) => moment(date).subtract(day - d, 'days').format('YYYY-MM-DD'))
  let daysAfter = daysAfterToday.map((d, idx) => moment(date).subtract(day - d, 'days').format('YYYY-MM-DD'))

  return daysBefore.concat(date.format('YYYY-MM-DD'), daysAfter)
}

const getState = (today) => {
  return {
    today: moment().format('YYYY-MM-DD'),
    daysOfWeek: getDaysOfWeek(today)
  }
}

const INITIAL_STATE = getState(now)

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case actions.PREV_WEEK:
      now.subtract(1, 'weeks')
      return getState(now)

    case actions.NEXT_WEEK:
      now.add(1, 'weeks')
      return getState(now)

    default:
      return state
  }
}
