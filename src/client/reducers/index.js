import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'

import uiReducer from './uiReducer'
import calendarReducer from './calendarReducer'
import usersReducer from './usersReducer'
import projectsReducer from './projectsReducer'
import tasksReducer from './tasksReducer'

export default combineReducers({
  routing: routerReducer,
  ui: uiReducer,
  calendar: calendarReducer,
  users: usersReducer,
  projects: projectsReducer,
  tasks: tasksReducer
})
