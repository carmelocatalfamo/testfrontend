import { combineReducers } from 'redux'

import * as actions from '../actions/types'

export default combineReducers({
  list: (state = [], action) => {
    switch (action.type) {
      case actions.GET_TASKS_LIST_SUCCEEDED:
        return action.tasks

      default:
        return state
    }
  },

  filter: (state = null, action) => {
    switch (action.type) {
      case actions.UPDATE_TASKS_FILTER:
        return action.filter

      default:
        return state
    }
  }
})
