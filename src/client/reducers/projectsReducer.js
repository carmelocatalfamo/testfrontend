import * as actions from '../actions/types'

export default (state = [], action) => {
  switch (action.type) {
    case actions.GET_PROJECTS_LIST_SUCCEEDED:
      return action.projects

    default:
      return state
  }
}
