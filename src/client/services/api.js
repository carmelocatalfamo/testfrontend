import axios from 'axios'

const BASE_URL = 'http://testfe.20tab.com/api'

export const fetchUsers = _ => {
  return axios.get(`${BASE_URL}/users/`)
    .then(function (result) {
      return result.data
    })
    .catch(error => {
      return { error }
    })
}

export const fetchProjects = _ => {
  return axios.get(`${BASE_URL}/team/projects/`)
    .then(function (result) {
      return result.data
    })
    .catch(error => {
      return { error }
    })
}

export const createProject = data => {
  return axios.post(`${BASE_URL}/team/projects/`, data)
    .then(function (result) {
      return result.data
    })
    .catch(error => {
      return { error }
    })
}

export const fetchTasks = _ => {
  return axios.get(`${BASE_URL}/team/tasks/`)
    .then(function (result) {
      return result.data
    })
    .catch(error => {
      return { error }
    })
}

export const createTask = data => {
  return axios.post(`${BASE_URL}/team/tasks/`, data)
    .then(function (result) {
      return result.data
    })
    .catch(error => {
      return { error }
    })
}
