import React, { Component } from 'react'
import { Route } from 'react-router-dom'
import styled from 'styled-components'

import Navbar from '../components/Navbar'
import Sidebar from '../components/Sidebar'
import Homepage from '../components/Homepage'
import Modal from '../components/Modal'

const RootContainer = styled.div`
  height: 100%;
  width: 100%;
`
const Content = styled.div`
  height: 100%;
  width: 100%;
  padding-top: 50px;
  display: flex;
  justify-content: center;
  position: relative;
`
const SidebarContainer = styled.div`
  width: 300px;
  height: calc(100% - 50px);
  position: fixed;
  bottom: 0px;
  left: 0px;
`
const MainContainer = styled.div`
  width: calc(100% - 300px);
  margin-left: 300px;
`

export default class Root extends Component {
  render () {
    return (
      <RootContainer>
        <Navbar />
        <Content>
          <SidebarContainer>
            <Sidebar />
          </SidebarContainer>
          <MainContainer>
            <Route exact path='/' component={Homepage} />
          </MainContainer>
        </Content>
        <Modal />
      </RootContainer>
    )
  }
}
