import { createStore, applyMiddleware, compose } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import { routerMiddleware } from 'react-router-redux'
import createSagaMiddleware from 'redux-saga'

import reducers from '../reducers'
import sagas from '../sagas'

let sagaMiddleware = createSagaMiddleware()

let composeEnhancers = (history) => {
  let middlewares = [sagaMiddleware, routerMiddleware(history)]
  let composeEnhancers = process.env.NODE_ENV === 'development' ? composeWithDevTools : compose
  return composeEnhancers(applyMiddleware(...middlewares))
}

export default function configureStore (initialState, history) {
  let store = createStore(reducers, initialState, composeEnhancers(history))
  let sagaTask = sagaMiddleware.run(sagas)

  // Hot realoder for reducers
  if (module.hot && process.env.NODE_ENV === 'development') {
    module.hot.accept('../reducers', () => {
      store.replaceReducer(reducers)
    })

    module.hot.accept('../sagas', () => {
      sagaTask.cancel()
      sagaTask.done.then(_ => {
        store = createStore(reducers, initialState, composeEnhancers())
        store.replaceReducer(reducers)
        sagaTask = sagaMiddleware.run(sagas)
      })
    })
  }

  return store
}
