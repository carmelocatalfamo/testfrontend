import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { ConnectedRouter } from 'react-router-redux'
import createHistory from 'history/createBrowserHistory'

import RootContainer from './containers/Root'
import configureStore from './store'

const MOUNT_NODE = document.getElementById('root')
const INITIAL_STATE = {}
const history = createHistory()
const store = configureStore(INITIAL_STATE, history)

const render = () => {
  ReactDOM.render(
    <Provider store={store}>
      <ConnectedRouter history={history}>
        <RootContainer />
      </ConnectedRouter>
    </Provider>,
    MOUNT_NODE
  )
}

if (module.hot && process.env.NODE_ENV === 'development') {
  module.hot.accept('./containers/Root', () => {
    ReactDOM.unmountComponentAtNode(MOUNT_NODE)
    render()
  })
}

render()
