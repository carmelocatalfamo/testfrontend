import { range, find, clone } from 'lodash'
import moment from 'moment'

// Filter tasks to find out task of actual week
export const findTasksOfThisWeek = (tasks, daysOfWeek) => {
  let firstValidDay = daysOfWeek[0]
  let lastValidDay = daysOfWeek[6]

  return tasks.filter(task => {
    let taskStartDate = moment(task.start_date)
    let taskEndDate = moment(task.end_date)
    let firstValidDate = moment(firstValidDay)
    let lastValidDate = moment(lastValidDay).add(1, 'days')

    return (taskStartDate.isAfter(firstValidDate) && taskEndDate.isBefore(lastValidDate)) ||
      (taskEndDate.isAfter(firstValidDate) && taskStartDate.isBefore(firstValidDate)) ||
      (taskStartDate.isBefore(lastValidDate) && taskEndDate.isAfter(lastValidDate))
  })
}

// Split every task to DOM element and push it in a single collection
// TODO: this should be splitted to functions
export const tasksToElements = (tasks, daysOfWeek, users, projects) => {
  return clone(tasks).reduce((collection, task) => {
    let firstValidDate = moment(daysOfWeek[0])
    let lastValidDate = moment(daysOfWeek[6])
    let taskStartDate = moment(task.start_date)
    let taskEndDate = moment(task.end_date)
    let fakeEndTime = null
    let fakeStartTime = null

    // If task if beetween 2 weeks
    if ((taskStartDate.isBefore(firstValidDate, 'days') || taskStartDate.isSame(firstValidDate, 'days')) && taskEndDate.isBefore(lastValidDate, 'days')) {
      taskStartDate = moment(firstValidDate)
      fakeStartTime = '09:00:00'
    } else if ((taskStartDate.isBefore(lastValidDate, 'days') || taskStartDate.isSame(lastValidDate, 'days')) && taskEndDate.isAfter(lastValidDate, 'days')) {
      taskEndDate = moment(lastValidDate)
      fakeEndTime = '20:00:00'
    }

    let diffDays = taskEndDate.diff(taskStartDate, 'days')
    let elements = range(0, diffDays + 1)
    let className = `${Math.random()}-${task.project}_${task.user}`

    let elementsForTask = elements.map(el => {
      let date = moment(taskStartDate).add(el, 'days')
      // Fix because Sun is 0 or 7, we use 7
      let day = date.day() || 7

      // Css grid raw
      let startHour = Number((fakeStartTime || task.start_time).split(':')[0])
      let endHour = Number((fakeEndTime || task.end_time).split(':')[0])
      let startRow = startHour <= 9 ? 2 : (startHour - 9 + 2)
      let endRow = endHour > 19 ? 'auto' : (endHour - 9 + 2)

      if (elements.length > 1) {
        if (!date.isSame(taskEndDate, 'day')) {
          endRow = 'auto'
        }

        if (!date.isSame(taskStartDate, 'day')) {
          startRow = 2
        }
      }

      let row = `${startRow} / ${endRow}`

      // Css grid column
      let column = `${day + 1} / ${day + 2}`

      return {
        user: find(users, ['id', task.user]),
        startDate: task.start_date,
        endDate: task.end_date,
        startTime: task.start_time,
        endTime: task.end_time,
        project: task.project,
        projectColor: task.project_color.includes('#') ? task.project_color : `#${task.project_color}`,
        column,
        row,
        className
      }
    })

    return collection.concat(elementsForTask)
  }, [])
}
