import React, { Component } from 'react'
import styled from 'styled-components'
import { connect } from 'react-redux'

import actions from '../actions'

const Container = styled.div`
  width: 100%;
  height: 50px;
  display: flex;
  position: fixed;
  justify-content: space-between;
  background-color: #363941;
  z-index: 10;
`
const Logo = styled.div`
  width: 300px;
  background: #5DC4C8;

  h2 {
    text-align: center;
    line-height: 50px;
    margin: 0px;
    color: white;
  }
`
const Nav = styled.ul`
  padding: 0px 15px;
  margin: 0px;
`
const MenuItem = styled.li`
  display: inline-block;
  list-style: none;
  color: white;
  cursor: pointer;
  margin: 10px;
  padding: 6px 5px;
  border-radius: 3px;

  &:hover {
    text-decoration: underline;
  }

  &.callToAction {
    background: #5cc4c9;
  }
`

class Navbar extends Component {
  render () {
    let { openModal } = this.props

    return (
      <Container>
        <Logo>
          <h2>Dashboard</h2>
        </Logo>
        <Nav>
          <MenuItem onClick={_ => openModal('addProject')}>New Project</MenuItem>
          <MenuItem className='callToAction' onClick={_ => openModal('addTask')}>New Task</MenuItem>
        </Nav>
      </Container>
    )
  }
}

export default connect(null, actions)(Navbar)
