import React, { Component } from 'react'
import { connect } from 'react-redux'
import styled from 'styled-components'
import moment from 'moment'
import Select from 'react-select'

import actions from '../../actions'
import DateRangePicker from '../Utils/DateRangePicker'

const Container = styled.div`
  width: 100%;
  max-width: 600px;
  padding: 35px;
  background: white;
  border-radius: 2px;
`
const Title = styled.h3`
  text-align: center;
  font-weight: 300;
  font-size: 26px;
  margin-top: 0px;
`
const Form = styled.form`
`
const InputGroup = styled.div`
  width: 100%;
  margin: 15px 0px;

  label {
    width: 100%;
    display: block;
    text-transform: uppercase;
    font-size: 12px;
    margin-bottom: 3px;
  }

  input {
    width: 100%;
    display: block;
    font-weight: 300;
    padding: 5px;
    outline: none;
  }

  .colors {
    width: 100%;
    display: flex;
    justify-content: space-between;
    align-items: center;

    button {
      height: 15px;
      width: 35px;
      border-radius: 3px;
      border: 1px solid #dfdfdf;
      cursor: pointer;
      outline: none;

      &:hover {
        border-color: #474c57;
      }
    }
  }
`
const Button = styled.button`
  background: #5DC4C8;
  border: none;
  border-radius: 3px;
  color: white;
  padding: 8px 15px;
  cursor: pointer;
  outline: none;
`

const isDayBlocked = (day, project) => {
  if (!project) return true

  let startDate = moment(project.startDate)
  let endDate = moment(project.endDate).add(1, 'days')
  return day.isBefore(startDate) || day.isAfter(endDate)
}

class AddTask extends Component {
  constructor (props) {
    super(props)
    this.state = {
      user: '',
      project: '',
      startTime: '',
      endTime: '',
      startDate: moment(),
      endDate: moment().add(1, 'days')
    }
  }

  submitForm (e) {
    e.preventDefault()
    this.props.createTask(this.state)
  }

  render () {
    let { users, projects } = this.props
    if (!users || !projects) return null

    let usersListToSelect = users.map(({ id, username }) =>
      ({ label: username, value: id }))
    let projectsListToSelect = projects.map(({ id, name, start_date, end_date }) =>
      ({ label: name, value: id, startDate: start_date, endDate: end_date }))

    return (
      <Container onClick={e => e.stopPropagation()}>
        <Title>Aggiungi una task</Title>
        <Form onSubmit={e => this.submitForm(e)}>
          <InputGroup>
            <label>User</label>
            <Select
              name='selectUser'
              value={this.state.user && this.state.user.value}
              onChange={(user) => this.setState({ user })}
              options={usersListToSelect}
            />
          </InputGroup>
          <InputGroup>
            <label>Project</label>
            <Select
              name='selectProject'
              value={this.state.project && this.state.project.value}
              onChange={(project) => this.setState({ project })}
              options={projectsListToSelect}
            />
          </InputGroup>
          <InputGroup>
            <label>Start time</label>
            <input
              type='text'
              placeholder='hh:mm'
              value={this.state.startTime}
              onChange={e => this.setState({ startTime: e.target.value })} />
          </InputGroup>
          <InputGroup>
            <label>End time</label>
            <input
              type='text'
              placeholder='hh:mm'
              value={this.state.endTime}
              onChange={e => this.setState({ endTime: e.target.value })} />
          </InputGroup>
          <InputGroup>
            <label>From date / To date</label>
            <DateRangePicker
              startDate={this.state.startDate}
              endDate={this.state.endDate}
              onDatesChange={({ startDate, endDate }) => this.setState({ startDate, endDate })}
              isDayBlocked={e => isDayBlocked(e, this.state.project)}
            />
          </InputGroup>
          <Button type='submit'>Crea</Button>
        </Form>
      </Container>
    )
  }
}

export default connect(state => {
  return {
    users: state.users,
    projects: state.projects
  }
}, actions)(AddTask)
