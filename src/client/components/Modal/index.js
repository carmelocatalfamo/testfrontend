import React, { Component } from 'react'
import { connect } from 'react-redux'
import styled from 'styled-components'

import actions from '../../actions'
import AddProject from './AddProject'
import AddTask from './AddTask'

const Background = styled.div`
  background: rgba(0, 0, 0, 0.5);
  height: 100%;
  width: 100%;
  position: fixed;
  z-index: 100;
  top: 0px;
  left: 0px;
  display: flex;
  justify-content: center;
  align-items: center;
  overflow: scroll;
  opacity: ${({ open }) => open ? 1 : 0};
  visibility: ${({ open }) => open ? 'visible' : 'hidden'};
  transition: 0.25s linear all;
`

const renderModal = type => {
  switch (type) {
    case 'addProject':
      return <AddProject />
    case 'addTask':
      return <AddTask />
    default:
      return null
  }
}

class Modal extends Component {
  render () {
    let { modalIsOpen, modalType, closeModal } = this.props

    return (
      <Background onClick={_ => modalType === 'paying' ? null : closeModal()} open={modalIsOpen}>
        {renderModal(modalType)}
      </Background>
    )
  }
}

export default connect(state => {
  return {
    modalIsOpen: state.ui.modal.open,
    modalType: state.ui.modal.type
  }
}, actions)(Modal)
