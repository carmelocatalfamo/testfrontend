import React, { Component } from 'react'
import { connect } from 'react-redux'
import styled from 'styled-components'
import moment from 'moment'

import actions from '../../actions'
import DateRangePicker from '../Utils/DateRangePicker'

const Container = styled.div`
  width: 100%;
  max-width: 600px;
  padding: 35px;
  background: white;
  border-radius: 2px;
`
const Title = styled.h3`
  text-align: center;
  font-weight: 300;
  font-size: 26px;
  margin-top: 0px;
`
const Form = styled.form`
`
const InputGroup = styled.div`
  width: 100%;
  margin: 15px 0px;

  label {
    width: 100%;
    display: block;
    text-transform: uppercase;
    font-size: 12px;
    margin-bottom: 3px;
  }

  input {
    width: 100%;
    display: block;
    font-weight: 300;
    padding: 5px;
    outline: none;
  }

  .colors {
    width: 100%;
    display: flex;
    justify-content: space-between;
    align-items: center;

    button {
      height: 15px;
      width: 35px;
      border-radius: 3px;
      border: 1px solid #dfdfdf;
      cursor: pointer;
      outline: none;

      &:hover {
        border-color: #474c57;
      }
    }
  }
`
const Button = styled.button`
  background: #5DC4C8;
  border: none;
  border-radius: 3px;
  color: white;
  padding: 8px 15px;
  cursor: pointer;
  outline: none;
`

const colors = ['#00b894', '#00cec9', '#0984e3', '#6c5ce7', '#fdcb6e', '#e17055', '#d63031', '#e84393', '#2d3436']

class AddProject extends Component {
  constructor (props) {
    super(props)
    this.state = {
      name: '',
      color: '',
      budget: '',
      supplier: '',
      startDate: moment(),
      endDate: moment().add(1, 'days')
    }
  }

  submitForm (e) {
    e.preventDefault()
    this.props.createProject(this.state)
  }

  render () {
    return (
      <Container onClick={e => e.stopPropagation()}>
        <Title>Aggiungi un progetto</Title>
        <Form onSubmit={e => this.submitForm(e)}>
          <InputGroup>
            <label>Name</label>
            <input type='text' value={this.state.name} onChange={e => this.setState({ name: e.target.value })} />
          </InputGroup>
          <InputGroup>
            <label>Budget</label>
            <input type='number' value={this.state.budget} onChange={e => this.setState({ budget: e.target.value })} />
          </InputGroup>
          <InputGroup>
            <label>Supplier</label>
            <input type='number' value={this.state.supplier} onChange={e => this.setState({ supplier: e.target.value })} />
          </InputGroup>
          <InputGroup>
            <label>Color</label>
            <div className='colors'>
              {colors.map(c =>
                <button
                  type='button'
                  key={c}
                  style={{
                    background: c,
                    borderColor: this.state.color === c ? '#474c57' : null,
                    height: this.state.color === c ? '20px' : null,
                    width: this.state.color === c ? '40px' : null
                  }}
                  onClick={_ => this.setState({ color: c })}
                />
              )}
            </div>
          </InputGroup>
          <InputGroup>
            <label>From date / To date</label>
            <DateRangePicker
              startDate={this.state.startDate}
              endDate={this.state.endDate}
              onDatesChange={({ startDate, endDate }) => this.setState({ startDate, endDate })}
            />
          </InputGroup>
          <Button type='submit'>Crea</Button>
        </Form>
      </Container>
    )
  }
}

export default connect(null, actions)(AddProject)
