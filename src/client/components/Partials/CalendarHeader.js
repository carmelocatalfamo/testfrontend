import React, { Component } from 'react'
import { connect } from 'react-redux'
import styled from 'styled-components'
import Select from 'react-select'
import ArrowLeftIcon from 'react-icons/lib/md/keyboard-arrow-left'
import ArrowRightIcon from 'react-icons/lib/md/keyboard-arrow-right'

import actions from '../../actions'

const Container = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 0px 15px;
`
const Filter = styled.div`
  font-weight: 300;
  text-transform: uppercase;
  color: #484848;
  margin: 25px 0px;
  width: 280px;
`
const Button = styled.button`
  cursor: pointer;
  color: #ffffff;
  background: #5cc4c9;
  border: none;
  border-radius: 3px;
  outline: none;
  transition: 0.2s ease background;
`

class CalendarHeader extends Component {
  constructor (props) {
    super(props)
    this.state = { user: '' }
  }

  render () {
    let { nextWeek, prevWeek, users, filter, updateFilter } = this.props

    if (!users) return null

    let usersListToSelect = users.map(({ id, username }) =>
      ({ label: username, value: id }))

    return (
      <Container>
        <Button onClick={_ => prevWeek()}>
          <ArrowLeftIcon />
        </Button>
        <Filter>
          <Select
            name='selectUser'
            placeholder='Select user to filter tasks'
            value={filter}
            onChange={(user) => updateFilter(user && user.value)}
            options={usersListToSelect}
          />
        </Filter>
        <Button onClick={_ => nextWeek()}>
          <ArrowRightIcon />
        </Button>
      </Container>
    )
  }
}

export default connect(state => {
  return {
    users: state.users,
    filter: state.tasks.filter
  }
}, actions)(CalendarHeader)
