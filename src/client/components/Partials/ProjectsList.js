import React, { Component } from 'react'
import { connect } from 'react-redux'
import styled from 'styled-components'

const Container = styled.ul`
  width: 100%;
  padding: 15px 0px;
  margin: 0px;
  height:
`
const Title = styled.h4`
  padding: 2px 35px;
  margin: 0px;
  margin-bottom: 15px;
  color: white;
  text-transform: uppercase;
  text-align: center;
  font-size: 14px;
  font-weight: 300;
`
const Project = styled.div`
  display: flex;
  align-items: center;
  padding: 5px 10px;
  transition: 0.2 ease background;

  .color {
    height: 15px;
    width: 35px;
    border-radius: 3px;
    border: 1px solid #474c57;
    margin-right: 10px;
  }

  p {
    margin: 0px;
    color: white;
  }
`

class ProjectsList extends Component {
  render () {
    let { projects } = this.props

    return (
      <Container>
        <Title>Projects Legend</Title>
        {projects.map(p =>
          <Project key={p.id}>
            <div className='color' style={{ backgroundColor: p.color.includes('#') ? p.color : `#${p.color}` }} />
            <p>{p.name}</p>
          </Project>
        )}
      </Container>
    )
  }
}

export default connect(state => {
  return {
    projects: state.projects
  }
})(ProjectsList)
