import React, { Component } from 'react'
import { connect } from 'react-redux'
import styled from 'styled-components'
import moment from 'moment'
import { range } from 'lodash'

import { findTasksOfThisWeek, tasksToElements } from '../../helpers/functions'

const Container = styled.div`
  height: calc(100% - 90px);
`
const Box = styled.div`
  text-align: center;
  color: #484848;
  text-transform: capitalize;
  font-weight: bold;
  align-self: center
`
const Grid = styled.div`
  display: grid;
  position: relative;
  grid-template-columns: repeat(8, auto);
  grid-template-rows: repeat(10, auto);
  margin: 0px;
  height: 100%;
`
const GridBlock = styled.div`
  background: white;
  color: grey;
  grid-row: ${({ raw }) => raw};
  grid-column: ${({ column }) => column};
  align-self: flex-start;
`
const Task = styled.div`
  position: absolute;
  color: white;
  opacity: 0.6;
  border-right: 1px solid white;
  background: ${({ background }) => background};
  grid-row: ${({ row }) => row};
  grid-column: ${({ column }) => column};
  top: 0px;
  left: 0px;
  right: 0px;
  bottom: 0px;
  padding: 5px;
`

const DaysOfWeekElements = ({daysOfWeek}) =>
  daysOfWeek.map(d => <Box key={d}>{moment(d).format('ddd DD')}</Box>)

const GridsBlock = _ =>
  range(-1, 11)
    .map((d, idx) => {
      if (d < 0) return <GridBlock key={d} raw={1} />

      d += 9
      return (
        <GridBlock
          key={d}
          raw={idx + 1}>
          {`${String(d).length === 1 ? '0' + d : d}:00`}
        </GridBlock>
      )
    })

const Tasks = ({ tasks }) =>
  tasks.map((t, idx) =>
    <Task
      className={t.className}
      onMouseEnter={_ => highlightTask(t.className, true)}
      onMouseLeave={_ => highlightTask(t.className, false)}
      key={`${t.user.id}_${idx}`}
      background={t.projectColor}
      row={t.row}
      column={t.column}>
      {t.user.username}
    </Task>
  )

const highlightTask = (className, active) => {
  let elementsToHighlight = document.getElementsByClassName(className)

  if (active) {
    Array.from(elementsToHighlight).map(el => {
      el.style.opacity = '1'
      el.style.zIndex = '10'
    })
  } else {
    Array.from(elementsToHighlight).map(el => {
      el.style.opacity = '0.6'
      el.style.zIndex = null
    })
  }
}

class Calendar extends Component {
  render () {
    let { daysOfWeek, tasks, users, filter } = this.props

    if (!tasks || !users) return null

    let filteredTasks = !filter ? tasks : tasks.filter(t => t.user === filter)
    let parsedTasks = findTasksOfThisWeek(filteredTasks, daysOfWeek)
    let elements = tasksToElements(parsedTasks, daysOfWeek, users)

    return (
      <Container>
        <Grid ref='grid'>
          <DaysOfWeekElements daysOfWeek={daysOfWeek} />
          <GridsBlock />
          <Tasks tasks={elements} />
        </Grid>
      </Container>
    )
  }
}

export default connect(state => {
  return {
    daysOfWeek: state.calendar.daysOfWeek,
    tasks: state.tasks.list,
    filter: state.tasks.filter,
    users: state.users
  }
})(Calendar)
