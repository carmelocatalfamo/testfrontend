import React, { Component } from 'react'
import styled from 'styled-components'

import ProjectsList from './Partials/ProjectsList'

const Container = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  background-color: #363941;
`

export default class Sidebar extends Component {
  render () {
    return (
      <Container>
        <ProjectsList />
      </Container>
    )
  }
}
