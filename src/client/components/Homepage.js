import React, { Component } from 'react'
import styled from 'styled-components'

import CalendarHeader from './Partials/CalendarHeader'
import Calendar from './Partials/Calendar'

const Container = styled.div`
  width: 100%;
  height: 100%;
`

export default class Homepage extends Component {
  render () {
    return (
      <Container>
        <CalendarHeader />
        <Calendar />
      </Container>
    )
  }
}
