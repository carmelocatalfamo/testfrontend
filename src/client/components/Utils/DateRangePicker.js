import 'react-dates/initialize'
import React, { Component } from 'react'
import styled from 'styled-components'
import { DateRangePicker } from 'react-dates'

const Container = styled.div`
  width: 100%;

  .DateRangePicker {
    width: 100%;

    .DateRangePickerInput {
      width: 100%;
      display: flex;
      justify-content: space-around;
      align-items: center;
    }
  }
`

export default class DateRangePickerCustomized extends Component {
  constructor (props) {
    super(props)
    this.state = {
      focusedInput: null
    }
  }

  render () {
    let { startDate, endDate, onDatesChange, isDayBlocked } = this.props

    return (
      <Container>
        <DateRangePicker
          startDate={startDate}
          startDateId='startDate'
          endDate={endDate}
          endDateId='endDate'
          onDatesChange={({ startDate, endDate }) => onDatesChange({ startDate, endDate })}
          focusedInput={this.state.focusedInput}
          onFocusChange={focusedInput => this.setState({ focusedInput })}
          isDayBlocked={isDayBlocked || (_ => false)}
          isOutsideRange={() => false}
        />
      </Container>
    )
  }
}
