import { call, all } from 'redux-saga/effects'

import usersSaga from './usersSaga'
import projectsSaga from './projectsSaga'
import tasksSaga from './tasksSaga'

export default function * root () {
  yield all([
    call(usersSaga),
    call(projectsSaga),
    call(tasksSaga)
  ])
}
