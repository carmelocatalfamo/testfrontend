import { take, call, all, put, fork } from 'redux-saga/effects'

import * as Api from '../services/api'
import * as action from '../actions/types'

function * getProjects () {
  const projects = yield call(Api.fetchProjects)

  if (projects.error) {
    yield put({ type: action.GET_PROJECTS_LIST_FAILED, error: projects.error })
  } else {
    yield put({ type: action.GET_PROJECTS_LIST_SUCCEEDED, projects })
  }
}

function * getProjectsFlow () {
  while (true) {
    yield take(action.GET_PROJECTS_LIST_REQUEST)
    yield fork(getProjects)
  }
}

function * createProjectFlow () {
  while (true) {
    let { data } = yield take(action.CREATE_PROJECT_REQUEST)

    let parsedProjectData = {
      name: data.name,
      color: data.color,
      budget: data.budget,
      supplier: data.supplier,
      start_date: data.startDate.format('YYYY-MM-DD'),
      end_date: data.endDate.format('YYYY-MM-DD')
    }

    const result = yield call(Api.createProject, parsedProjectData)

    if (result.error) {
      yield put({ type: action.CREATE_PROJECT_FAILED, error: result.error })
      continue
    }

    yield put({ type: action.CREATE_PROJECT_SUCCEEDED, projectCreated: result })
    yield put({ type: action.CLOSE_MODAL })
    yield fork(getProjects)
  }
}

export default function * root () {
  yield all([
    call(getProjects),
    call(getProjectsFlow),
    call(createProjectFlow)
  ])
}
