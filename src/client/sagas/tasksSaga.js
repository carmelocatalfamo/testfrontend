import { take, call, all, put, fork } from 'redux-saga/effects'

import * as Api from '../services/api'
import * as action from '../actions/types'

function * getTasks () {
  const tasks = yield call(Api.fetchTasks)

  if (tasks.error) {
    yield put({ type: action.GET_TASKS_LIST_FAILED, error: tasks.error })
  } else {
    yield put({ type: action.GET_TASKS_LIST_SUCCEEDED, tasks })
  }
}

function * getTasksFlow () {
  while (true) {
    yield take(action.GET_TASKS_LIST_REQUEST)
    yield fork(getTasks)
  }
}

function * createTaskFlow () {
  while (true) {
    let { data } = yield take(action.CREATE_TASK_REQUEST)

    let parsedTaskData = {
      user: data.user.value,
      project: data.project.value,
      start_date: data.startDate.format('YYYY-MM-DD'),
      end_date: data.endDate ? data.endDate.format('YYYY-MM-DD') : data.startDate.format('YYYY-MM-DD'),
      start_time: data.startTime,
      end_time: data.endTime
    }

    const result = yield call(Api.createTask, parsedTaskData)

    if (result.error) {
      yield put({ type: action.CREATE_TASK_FAILED, error: result.error })
      continue
    }

    yield put({ type: action.CREATE_TASK_SUCCEEDED, taskCreated: result })
    yield put({ type: action.CLOSE_MODAL })
    yield fork(getTasks)
  }
}

export default function * root () {
  yield all([
    call(getTasks),
    call(getTasksFlow),
    call(createTaskFlow)
  ])
}
