import { call, all, put } from 'redux-saga/effects'

import * as Api from '../services/api'
import * as action from '../actions/types'

function * getUsersFlow () {
  const users = yield call(Api.fetchUsers)

  if (users.error) {
    yield put({ type: action.GET_USERS_LIST_FAILED, error: users.error })
    return
  }

  yield put({ type: action.GET_USERS_LIST_SUCCEEDED, users })
}

export default function * root () {
  yield all([
    call(getUsersFlow)
  ])
}
