import * as uiActions from './uiActions'
import * as calendarActions from './calendarActions'
import * as projectActions from './projectActions'
import * as taskActions from './taskActions'

export default Object.assign(
  {},
  uiActions,
  calendarActions,
  projectActions,
  taskActions
)
