import * as actions from './types'

export const createTask = data => {
  return {
    type: actions.CREATE_TASK_REQUEST,
    data
  }
}

export const updateFilter = filter => {
  return {
    type: actions.UPDATE_TASKS_FILTER,
    filter
  }
}
