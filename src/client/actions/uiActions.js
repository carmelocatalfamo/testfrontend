import * as actions from './types'

export const openModal = modalType => {
  return {
    type: actions.OPEN_MODAL,
    modal: modalType
  }
}

export const closeModal = _ => {
  return {
    type: actions.CLOSE_MODAL
  }
}
