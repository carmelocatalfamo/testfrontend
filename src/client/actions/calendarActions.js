import * as actions from './types'

export const nextWeek = _ => {
  return {
    type: actions.NEXT_WEEK
  }
}

export const prevWeek = _ => {
  return {
    type: actions.PREV_WEEK
  }
}
