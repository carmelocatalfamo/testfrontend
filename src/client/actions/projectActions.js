import * as actions from './types'

export const createProject = data => {
  return {
    type: actions.CREATE_PROJECT_REQUEST,
    data
  }
}
