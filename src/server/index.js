const express = require('express')
const history = require('connect-history-api-fallback')
const path = require('path')

const app = express()

app.use(history())

app.use('/static', express.static(path.resolve('src/server/static')))

if (process.env.NODE_ENV !== 'production') {
  let webpackMiddleware = require('./middlewares/webpack')
  webpackMiddleware(app)
}

app.listen('3000', _ => console.log('App listening on port 3000!'))
