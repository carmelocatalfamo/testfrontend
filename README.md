# Test Frontend

![alt text](https://i.gyazo.com/63ae3a688c0fba4a33a3f9104a682a41.png)

## Setup
Download repository
```shell
$ git clone https://carmelocatalfamo@bitbucket.org/carmelocatalfamo/testfrontend.git
```

Installazione dipendenze
```shell
$ npm install
```

Start
```shell
$ npm run dev
```

L'application sarà in ascolto su `http://localhost:3000`

## Progetto
Ho sviluppato questo progetto dividendolo in due cartelle principali:
* src/server
* src/client

Per quanto riguarda la parte del server non c'è molto da dire, solitamente preferisco usare express al posto del server di webpack, lo trovo molto più customizzabile.

La cartella client invece contiene:

* actions: in cui vengono conservate tutte le azione che leggono o modificano lo stato dell'applicatione, suddivisi in base al loro contesto.
* components: la cartella dei componenti di React
* containers: qui conservo i componenti di root dell'applicazione e a meno che non siano progetti molto complessi il file è quasi sempre uno.
* helpers: cartella delle funzioni che possono essere utili per l'applicazione.
* reducers e store: reducers di Redux
* sagas e services: tutte le sagas dell'applicazione, mi permettono di tenere nello strato più esterno tutte le azione che hanno dei side effetcs.

## Difficoltà
L'unico problema che ho avuto sono stati i **CORS**, il server non accettava le mie chiamate alle API con questo errore:

```
No 'Access-Control-Allow-Origin' header is present on the requested resource.
Origin 'http://localhost:3000' is therefore not allowed access.
```

Ho provato con diverse opzioni nell'header della richiesta ma senza successo, non volendo passare le API lato server (con cui in realtà funzionava) ho optato per un plugin di chrome che mi permetteva di 'aggirare' l'errore.

## To-do
* Media queries per il mobile
* Inserimento di un time picker
* Validazione dei campi del form delle tasks e del form dei progetti
* Output errori durante l'invio dei form
* Le task rappresentate nel calendario non prendono in considerazione i minuti nella loro disposizione (Esempio: se una task inizia alle 9:30 o alle 9:45, nel tabellone viene visualizzata alle ore 9:00)
